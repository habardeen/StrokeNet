import os
import fnmatch
import shutil


# Destination and source dirs
DST_DIR = './Data/Flair/'
SRC_DIR = '/home/habardeen/neural/ISLES2015/SISS2015/'

# Train and label dirs + format
TYPE1_DIR = ''
LABEL_DIR = ''
IMG_FORMAT = '.nii'

# Image types
TYPE1 = 'Flair'
TYPE2 = 'T2'
LABEL = 'OT'

# Travel recursively from source
for path, dirs, files, in os.walk(SRC_DIR):

    for file in files:

        dst_root = None

        if fnmatch.fnmatch(file, '*' + TYPE1 + '*' + IMG_FORMAT):
            dst_root = DST_DIR + TYPE1_DIR
            dst = dst_root + TYPE1 + '_'
        elif fnmatch.fnmatch(file, '*' + LABEL + '*' + IMG_FORMAT):
            dst_root = DST_DIR + LABEL_DIR
            dst = dst_root + LABEL + '_'
        
        # If matching file was found, copy and rename
        if dst_root is not None:
            if not os.path.exists(dst_root):
                os.makedirs(dst_root)
            src = os.path.abspath(os.path.join(path, file))
            par_dir = os.path.abspath(os.path.join(path, os.pardir))
            par_par_dir = os.path.abspath(os.path.join(par_dir, os.pardir))
            dst += par_dir.replace(par_par_dir + '/', '') + IMG_FORMAT
            dst = dst.replace('_0', '_')
            print(dst)
            shutil.copy2(src, dst)

