# StrokeNet

This project aims to classify and predict patient outcomes from stroke data, consisting of several types of MRI, to inform neurologists and radiologists of optimal treatment pathways.


## Status

- 20/05/2018: Further tuning the general model.
- 04/04/2018: Fixing training for whole dataset. 
- 25/02/2018: Overfitting one slice on Unet.


## First Results Examples

First training result (all slices): **0.41 DSC**, average on 24 unseen patients. <br /><br /> 
Best results: <br />
![](images/patient9.png)
![](images/patient15.png)
![](images/patient23.png)
![](images/patient28.png)


## Getting Started

1. You can find the data here: http://www.isles-challenge.org/ISLES2015/
2. Unpack raw data in corresponding folders
3. Setup the network in Run.py and uncomment the desired task from the Main section at the end


## Credits

- Andrea Schwendimann (Author) 
- Zach Eaton-Rosen, UCL Engineering (Contributor)


## References

- Chen, J. et al. (2016). Combining Fully Convolutional and Recurrent Neural Networks for 3D Biomedical Image Segmentation. https://arxiv.org/abs/1609.01006
- Ronneberger, J., Fischer, P., Brox, T. (2015). U-Net: Convolutional Networks for Biomedical Image Segmentation. https://arxiv.org/abs/1505.04597
- Sudre C. H. et al. (2017). Generalised Dice Overlap as Deep Learning Loss Function for Highly Unbalanced Segmentations. https://arxiv.org/pdf/1707.03237.pdf
