import numpy as np
import nibabel as nib
from keras.backend import int_shape
from keras.layers import Cropping2D, Concatenate, ZeroPadding2D


# Settings
data_split = (23, 4, 1)         # train, val, test
work_dir = './Data/'
scan_type = 'Flair'
label_file = 'OT'
ext = '.nii'

# Network Weight Matrix
class_weight_zero = 1.
class_weight_one = 50.


# Reshape label and initialise network weights
def get_weights(label, num_patients = 1, num_channels = 1):
    label = label[:-1, :-1]
    label_slice = np.copy(label).reshape(label.shape[0] * label.shape[1], num_channels)
    weights = np.copy(label)
    weights[weights == 0.] = class_weight_zero
    weights[weights == 1.] = class_weight_one
    weights = weights.reshape(num_patients, label.shape[0] * label.shape[1])
    return label_slice, weights


# Yield one slice from an image
def slice_generator(batch_size, data_range, report = False):
    while 1:

        # Shuffle patients
        samples = np.arange(data_range[0], data_range[1])
        np.random.shuffle(samples)

        for k in range(samples.shape[0]):
            if report:
                print('\n\n********************************')
                print('*    Training on patient # ' + str(samples[k]))
                print('********************************')

            train = nib.load(work_dir + scan_type + '/' + scan_type + '_' + str(samples[k]) + ext).get_data()
            label = nib.load(work_dir + 'Label/' + label_file + '_' + str(samples[k]) + ext).get_data()

            #####################
            # Data augmentation #
            #####################

            # Flip brain on random axis
            if np.random.randint(1, 101) % 2 == 0:
                if report:
                    print('==> Flipping brain...')
                axis = np.random.randint(0, len(train.shape))
                train = np.flip(train, axis=axis)
                label = np.flip(label, axis=axis)

            # Shuffle slices
            if np.random.randint(1, 101) % 2 == 0:
                if report:
                    print('==> Shuffling slices...')
                rng_state = np.random.get_state()
                np.random.shuffle(train)
                np.random.set_state(rng_state)
                np.random.shuffle(label)

            for i in range(0, train.shape[2], batch_size):
                label_slice, weights = get_weights(label[..., i])
                yield train[None, ..., i, None], label_slice[None, ...], weights


# Pad a tensor with zeros to reach target_dim
def padding2D(img, target_dim=(324,324), data_format='channels_last'):

    # Padding dimensions (2-sides)
    pad_X = target_dim[0] - int_shape(img)[1]
    pad_Y = target_dim[1] - int_shape(img)[2]

    # Add one column from left/bottom if odd
    if pad_X % 2 != 0:
        img = ZeroPadding2D(padding=((0, 0), (1, 0)), data_format=data_format)
        pad_X -= 1
    if pad_Y % 2 != 0:
        img = ZeroPadding2D(padding=((0, 1), (0, 0)), data_format=data_format)
        pad_Y -= 1

    # Padding dimensions (1-side)
    pad_X = pad_X // 2
    pad_Y = pad_Y // 2

    img = ZeroPadding2D(padding=(pad_X, pad_Y), data_format=data_format)(img)
    return img


# Crop Unet encoder tensor and concatenate to decoder
def crop_concat(decoder, features, data_format='channels_last'):

    # Cropping dimesions (2-sides)
    crop_X = int_shape(features)[1] - int_shape(decoder)[1]
    crop_Y = int_shape(features)[2] - int_shape(decoder)[2]

    # Remove one pixel from left/bottom if odd
    if crop_X % 2 != 0:
        features = Cropping2D(cropping=((0, 0), (1, 0)), data_format=data_format)(features)
        crop_X -= 1
    if crop_Y % 2 != 0:
        features = Cropping2D(cropping=((0, 1), (0, 0)), data_format=data_format)(features)
        crop_Y -= 1

    # Cropping dimensions (1-side)
    crop_X = crop_X // 2
    crop_Y = crop_Y // 2

    features = Cropping2D(cropping=(crop_Y, crop_X), data_format=data_format)(features)
    decoder = Concatenate(axis=3)([features, decoder])
    return  decoder