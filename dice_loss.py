import numpy as np
import keras.backend as K

# Modified from: https://stackoverflow.com/questions/45961428/make-a-custom-loss-function-in-keras
def dice_coeff(y_true, y_pred, smooth=1e-5):
    if y_pred.shape[-1] == 2:
        # only look at the probabilities for 'true'
        y_pred = y_pred[..., -1]

    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)


# For np.ndarray instead of K.tensor
def dice_coeff_numpy(y_true, y_pred, smooth=1e-5):
    y_true_f = y_true.flatten()
    y_pred_f = y_pred.flatten()
    intersection = np.sum(y_pred_f * y_true_f)
    return (2. * intersection + smooth) / (np.sum(y_true) + np.sum(y_pred) + smooth)


# Wrapper for Keras
def dice_loss(smooth=1e-5):
  def dice(y_true, y_pred):
    return -dice_coeff(y_true, y_pred, smooth)
  return dice