import transform as tran

from keras.models import Model
from keras.layers import Conv2D, UpSampling2D
from keras.layers import MaxPooling2D, BatchNormalization
from keras.layers import Dropout, GaussianDropout, AlphaDropout
from keras.layers.advanced_activations import LeakyReLU, PReLU
#from keras.layers import Conv2DTranspose


# Parameters
activ_out = 'softmax'                   # activation function for last convolution
drop_type = 'dropout'                   # [Dropout = dropout, GaussianDrpout = gauss, AlphaDropout = alpha]
dropout_rate = 0.5                      # amount of input units affected

channels = [64, 128, 256, 512]          # feature channels (input_layer + depth)
kernel_size = (3, 3)                    # intra-layer kernel size
up_kernel_size = (2, 2)                 # decoder inter-layer kernel size
max_pool_size = (2, 2)                  # encoder inter-layer kernel size
initializer = 'he_normal'               # kernal/alpha matrix initializer



# Dropout layer choice
def dropout(tensor, drop):

    # Defaults to normal Dropout()
    if drop == 'alpha':
        tensor = AlphaDropout(dropout_rate)(tensor)
    elif drop == 'gauss':
        tensor = GaussianDropout(dropout_rate)(tensor)
    else:
        tensor = Dropout(dropout_rate)(tensor)

    return tensor


# U-NET (as in: https://arxiv.org/abs/1505.04597)
def unet(inp, keras_model = False):

    crop_features = []
    encoder = inp

    # Encoder
    for depth_, filter_ in enumerate(channels):
        encoder = Conv2D(filter_, kernel_size, kernel_initializer=initializer)(encoder)
        encoder = PReLU(alpha_initializer=initializer)(encoder)
        encoder = dropout(encoder, drop_type)
        encoder = Conv2D(filter_, kernel_size, kernel_initializer=initializer)(encoder)
        encoder = PReLU(alpha_initializer=initializer)(encoder)

        # Skip pooling for last layer
        if depth_ != len(channels) -1:
            crop_features.append(encoder)
            encoder = MaxPooling2D(max_pool_size, padding='same')(encoder)

    # Decoder
    decoder = encoder
    for depth_, filter_ in reversed(list(enumerate(channels))):

        if depth_ == len(channels) - 1:
            continue

        # Up-convolution (could use Conv2DTransposed)
        decoder = UpSampling2D(size=up_kernel_size)(decoder)
        decoder = Conv2D(filter_, up_kernel_size, kernel_initializer=initializer)(decoder)
        decoder = PReLU(alpha_initializer=initializer)(decoder)

        # Crop, concatenate and convolve
        decoder = tran.crop_concat(decoder, crop_features[depth_])
        decoder = Conv2D(filter_, kernel_size, kernel_initializer=initializer)(decoder)
        decoder = PReLU(alpha_initializer=initializer)(decoder)
        decoder = dropout(decoder, drop_type)
        decoder = Conv2D(filter_, kernel_size, kernel_initializer=initializer)(decoder)
        decoder = PReLU(alpha_initializer=initializer)(decoder)

    out = Conv2D(2, 1, activation=activ_out, kernel_initializer=initializer)(decoder)

    # Output
    if keras_model:
        return Model(inputs=inp, outputs=out)
    else:
        return out

