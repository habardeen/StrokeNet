import os
import numpy as np
import tensorflow as tf
import logging, sys
import pickle

from keras.utils import plot_model
from keras.models import Model, load_model
from keras.layers import Input, BatchNormalization, Reshape, Lambda
from keras.callbacks import ModelCheckpoint
from keras.optimizers import Adam
from keras.backend import set_session, clear_session

import unet, logger
import transform as tran
import dice_loss as dice
import nibabel as nib
import matplotlib.pyplot as plt


##########################################
#               SETTINGS                 #
##########################################

# General
print_graph = False
enable_log = False
CPU_only = False
CPU_num = 1
CPU_threads = 8
GPU_mem_tuning = False
GPU_mem_perc = 0.98
np.random.seed(1337)

# Inner
batch_size1 = 1
epochs1 = 420             # checkpointing enabled for epochs > 100 on non_overfit
loss = 'dice'
dice_smooth = 1e-5
optimizer1 = 'adam'     # defaults to SGD
learn_rate = 4e-6
epsilon = 1e-10
decay = 0.0             # learn_rate decrease with each epoch
base_thresh = 0.81       # base threshold for plotting
pred_slice = 120        # Prediction slice number
pred_patient = 26        # Prediction patient number

# Inner Model Files
overfit_path = './Output/overfit_patient'  + str(pred_patient) + '_slice' + str(pred_slice) + '.h5'
overfit_all_path = './Output/overfit_all/overfit_patient.h5'
train_path = './Output/unet_epochs' + str(epochs1) + '.h5'
history_path = 'history_epochs' + str(epochs1)
img_path = 'generalised/'

select_path = './Output/Best/generalized_epochs2_patients4.h5'
select_history = './Output/ONE_history_epochs10'

# Load data info
all_manual = [95, 83, 72, 90, 73, 73, 87, 103, 70, 81, 82, 77, 93, 101, 77,
              109, 102, 135, 91, 18, 74, 56, 82, 119, 121, 48, 28, 92]
patients_num = 28
patients_train = 23
patients_val = patients_num - patients_train - 1
X_dim = 230
Y_dim = 230
Z_dim = 154
tot_train_slices = Z_dim

# data_info = nib_ops.get_data_info(1)       # only if nib_ops module is available
# data_info = {                               # hard-coded (ISLES specific)
#         'shape': (230, 230, 154),
#         'voxel_size': (1.0, 1.0, 1.0),
#         'dtype': '<f4',                     # little-endian float32
#         'patients_tot': 28,
#         'patients_train': 23,
#         'patients_val': 4,
#         'patients_test': 1,
#         'work_dir': './Data/',
#         'scan_type': 'Flair',
#         'label': 'OT',
#         'ext': '.nii' }

# Hardware
if CPU_only:
    config = tf.ConfigProto(intra_op_parallelism_threads=CPU_threads,
            inter_op_parallelism_threads=CPU_threads, allow_soft_placement=True,
            device_count = {'CPU' : CPU_num, 'GPU' : 0})
    sess = tf.Session(config=config)
    set_session(sess)
elif GPU_mem_tuning:
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=GPU_mem_perc)
    sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
    set_session(sess)

# Logger
if enable_log:
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(levelname)s %(message)s',
        handlers=[ logging.FileHandler(r'./err.log')])
    sys.stderr = logger.LogFile('stderr')



##########################################
#  StrokeNet1 (one Unet for all slices)  #
##########################################

def StrokeNet1(overfit = False, slice_ = pred_slice, patient = pred_patient):
    # Input shape: (X, Y, Channels)
    inp = Input(shape=(X_dim, Y_dim, 1))

    # Normalize (activation is linear => no scaling)
    norm = BatchNormalization(scale=False)(inp)

    # Pad (230x230->324x324) and get model
    pad = tran.padding2D(norm)
    out = unet.unet(pad)

    # Slice False dimension and reshape to 2D output (to use custom weights init)
    out = Lambda(lambda x: x[..., -1])(out)
    out = Reshape(((X_dim - 1) * (Y_dim - 1), 1))(out)
    model = Model(inputs=inp, outputs=out)

    print(model.summary())
    StrokeNet1_compile(model, slice_, patient, overfit)


def StrokeNet1_compile(model, slice_ = pred_slice, patient = pred_patient, overfit = False):
    if print_graph:
        plot_model(model, to_file='./Output/unet.png')

    # Metrics
    if loss == 'dice':
        curr_loss = dice.dice_loss(dice_smooth)
        metrics = [dice.dice_coeff]
    else:
        curr_loss = 'binary_crossentropy'
        metrics = ['binary_accuracy']

    # Optimizer
    if optimizer1 == 'adam':
        optimizer = Adam(lr=learn_rate, epsilon=epsilon, decay=decay)
    else:
        optimizer = 'sgd'

    print('Compiling Unet...')
    model.compile(optimizer=optimizer, loss=curr_loss, metrics=metrics, sample_weight_mode='temporal')

    if overfit:
        StrokeNet1_overfit(model, slice_, patient)
    else:
        StrokeNet1_train(model)


# Get segmentation and DCS
def StrokeNet1_segment(y_truth, pred):
    dice_arr = np.zeros(9)
    threshold = []
    for i in range(dice_arr.shape[0]):
        pred_filtered = np.copy(pred)
        y_pred = pred_filtered.flatten()
        if i == 0:
            thresh = base_thresh
        else:
            thresh = base_thresh + i / 100
        threshold.append(thresh)
        y_pred[y_pred <= threshold[i]] = 0.
        dice_arr[i] = dice.dice_coeff_numpy(y_truth, y_pred)

    print('\nBest DCS: %.4f' % np.amax(dice_arr))
    print('Best DCS Step: %i' % np.argmax(dice_arr))

    index = np.argmax(dice_arr)

    try:
        thresh_max = threshold[index]
    except:
        print('\nExpected int-type for max threshold, found ' + str(type(index)) + '\n')
        raise

    pred_filtered = np.copy(pred)
    pred_filtered[pred_filtered <= thresh_max] = 0.

    return pred_filtered, np.amax(dice_arr)


# Save history and model
def StrokeNet1_save(model, history, z_slice = pred_slice, patient = pred_patient, overfit = False):
    global history_path
    path = history_path
    if overfit:
        main_path = './Output/'
        model_path = overfit_all_path
        path += '_patient' + str(patient) + '_slice' + str(z_slice) + '_overfit'
    else:
        main_path = './Output/'
        model_path = train_path

    with open(main_path + path, 'wb') as file_p:
        pickle.dump(history.history, file_p)
    model.save(model_path)
    print('==> SAVED model as: ' + model_path)
    print('==> SAVED history as: ' + main_path + path)


# Train on all slices
def StrokeNet1_train(model):
    # Generators
    generator_tr = tran.slice_generator(batch_size1, (1, patients_train + 1), report=True)
    generator_val = tran.slice_generator(batch_size1, (patients_train + 1, patients_train + patients_val + 1))
    steps = (tot_train_slices * patients_train) // batch_size1
    steps_val = (tot_train_slices * patients_val) // batch_size1

    # Callbacks
    callbacks_list = None
    if epochs1 > 100:
        file_path = './Output/checkpoint_epochs' + str(epochs1) + '.h5'
        checkpoint = ModelCheckpoint(file_path, monitor='val_dice_loss', verbose=1, save_best_only=True, mode='max')
        callbacks_list = [checkpoint]

    # If using multiprocessing, do not use generators -> Use data sequences.
    # Source: https://keras.io/utils/#sequence
    history = model.fit_generator(generator_tr, validation_data=generator_val, validation_steps=steps_val, max_queue_size=1,
                        epochs=epochs1, steps_per_epoch=steps, callbacks=callbacks_list,
                        use_multiprocessing=False, shuffle=False)
    StrokeNet1_save(model, history)


# Overfit one slice
def StrokeNet1_overfit(model, z_slice, patient):
    img = nib.load('./Data/Flair/Flair_' + str(patient) + '.nii').get_data()
    truth = nib.load('./Data/Label/OT_' + str(patient) + '.nii').get_data()

    truth, weights = tran.get_weights(truth[..., z_slice])

    history = model.fit(x=img[None, ..., z_slice, None], y=truth[None, ...], epochs=epochs1)#, sample_weight=weights)
    StrokeNet1_save(model, history, z_slice, patient, True)


# Predict from a trained model
def StrokeNet1_load_plot(model_path, z_slice = None, patient = None):
    model = load_model(model_path, custom_objects={'dice': dice.dice_loss(dice_smooth), 'dice_coeff': dice.dice_coeff})

    if z_slice is None and patient is None:
        for patient_, slice_ in enumerate(all_manual, 1):
            predict(model, model_path, slice_, patient_)
    else:
        predict(model, model_path, z_slice, patient)


def predict(model, model_path, z_slice, patient):
    img = nib.load('./Data/Flair/Flair_' + str(patient) + '.nii').get_data()
    truth = nib.load('./Data/Label/OT_' + str(patient) + '.nii').get_data()
    truth = truth[:-1, :-1, :]
    pred = model.predict(img[None, ..., z_slice, None])

    pred = pred.reshape((X_dim - 1), (Y_dim - 1), 1)
    y_truth = truth[..., z_slice].flatten()
    pred_filtered, err = StrokeNet1_segment(y_truth, pred)
    print(np.count_nonzero(truth[..., z_slice].flatten()))
    print(np.count_nonzero(pred_filtered.flatten()))
    #StrokeNet1_plot(model_path, pred_filtered, err, patient, z_slice, truth, pred, img_path)


# Plot brain slice
def StrokeNet1_plot(model_path, pred_filtered, err, patient, z_slice, truth, pred, save_path = ''):
    fig, ax = plt.subplots()
    title = 'Dice Score: %.4f' % err
    sub_title = 'Patient ' + str(patient) + ', Slice ' + str(z_slice)

    ax.cla()
    ax.imshow(np.hstack([truth[..., z_slice], pred.squeeze(), pred_filtered.squeeze()]), vmin=0, vmax=1)
    ax.set_title(sub_title, fontsize=12)
    plt.tick_params(axis='both', which='both', bottom='off', top='off', labelbottom='off', right='off',
                    left='off', labelleft='off')

    fig.tight_layout()
    fig.get_axes()[0].annotate(title, (0.5, 0.836), xycoords='figure fraction', ha='center', fontsize=18)
    plt.figtext(0.18, 0.25, 'Ground Truth', wrap=True, ha='center', fontsize=11)
    plt.figtext(0.48, 0.25, 'Raw Output', wrap=True, ha='center', fontsize=11)
    plt.figtext(0.81, 0.25, 'Segmentation', wrap=True, ha='center', fontsize=11)

    save_path = './Output/' + save_path + 'patient' + str(patient) #+ '_slice' + str(z_slice)
    if model_path == overfit_path:
        save_path += '_overfit'

    # Save only if label and prediction are non-empty
    if np.count_nonzero(truth[..., z_slice].flatten()) != 0 and np.count_nonzero(pred_filtered.flatten()) != 0:
        fig.savefig(save_path, bbox_inches='tight', pad_inches=0)
        print('==> SAVED brain as: ' + save_path)
        return True
    else:
        return False


# Plot model history
def plot_history(path, plot_val = False):
    with open(path, 'rb') as pickle_file:
        history = pickle.load(pickle_file)

    print(history.keys())

    plt.plot(history['dice_coeff'])
    plt.title('Dice Accuracy')
    plt.ylabel('dice score')
    plt.xlabel('epoch')

    if plot_val:
        plt.plot(history['val_dice_coeff'])
        plt.legend(['training', 'validation'])

    plt.show()

def train_from_loaded(path):
    model = load_model(path, custom_objects={'dice': dice.dice_loss(dice_smooth), 'dice_coeff': dice.dice_coeff})
    config_ = model.get_config()
    model = Model.from_config(config_)

    print(model.summary())

    StrokeNet1_compile(model)


##########################################
#  StrokeNet2                            #
##########################################

# TODO LSTM for inter-slice segmentation

def StrokeNet2():
    print('\nNot implemented yet')



##########################################
#                 Main                   #
##########################################

def main():
    # Overfit
    # for patient, slice_ in enumerate(all_manual, 1):
    #
    #     print('\n ############# PATIENT ' + str(patient) + ', SLICE ' + str(slice_))
    #
    #     StrokeNet1(True, slice_, patient)
    #     StrokeNet1_predict_slice(overfit_all_path, slice_, patient)
    #     clear_session()
    #     os.remove(overfit_all_path)

    # StrokeNet1(True)
    # StrokeNet1_predict_slice(overfit_all_path, pred_slice, pred_patient)
    # #os.remove(overfit_all_path) # Delete the model if overwrite for batch predictions

    # Full training
    #StrokeNet1()
    #StrokeNet1_predict_slice(train_path, pred_slice, pred_patient)

    # Predict ONE using specific model
    StrokeNet1_load_plot(select_path, pred_slice, pred_patient)

    # Predict ALL patients on specific model
    # StrokeNet1_load_plot(select_path)

    # Plot history
    # plot_history(select_history, True)


if __name__ == '__main__':
    main()